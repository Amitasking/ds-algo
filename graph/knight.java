import java.util.Queue;
import java.util.LinkedList;

class knight{
   static class Node{
       int x;
       int y;
       int d;
   }



   static boolean isValid(int x, int y, int N)  
   {  
       if (x >= 1 && x <= N && y >= 1 && y <= N)  
           return true;  
       return false;  
   }  

    public static void bfs(int N){
        Queue<Node> q = new LinkedList<>();
        int[][] visited = new int[N+1][N+1];
        Node n = new Node();
        n.x = 1;
        n.y = 1;
        n.d  = 0;
        visited[n.x][n.y] = 1;
        q.add(n);
        int dx[] = {-2, -1, 1, 2, -2, -1, 1, 2};  
        int dy[] = {-1, -2, -2, -1, 1, 2, 2, 1};  

        while(!q.isEmpty()){
            n = q.remove();
            System.out.println(n.x + " " + n.y);

            for(int i = 0; i < 8 ; i++){
                int x = n.x + dx[i];
                int y = n.y + dy[i];
                
                if(isValid(x,y, N) && visited[x][y] != 0){
                    visited[x][y] = 1;
                    Node t = new Node();
                    t.x = x;
                    t.y = y;
                    t.d = t.d + 1;
                    q.add(t);
                }
            }


        }

        
    }
   
  
   
   
    public static void main(String[] args) {
        int n = 30;
        bfs(n);
    }
}