import java.util.*;

class prims{


    public static int findMinVertex(int[] weight, int[] visited){
            int minVertex = -1;
            int min = Integer.MAX_VALUE;

            for(int i = 0; i < weight.length; i++){
                if(weight[i] < min && visited[i] != 1){
                    min = weight[i];
                    minVertex = i;}
            }
          return minVertex;
    }


public static void prim(int n, int g[][]){

    int[] parent = new int[n];
    int[] weight = new int[n];
    int[] visited = new int[n];

    //set weights to ifinity

    for(int i = 0; i < n; i++){
        visited[i] = 0;
        weight[i] = Integer.MAX_VALUE; }

    //starting vertex
    parent[0] = -1;
    weight[0] = 0;

    for(int i = 0; i < n; i++){
       
        int minVertex =  findMinVertex(weight, visited);
        visited[minVertex] = 1;
        //explore neigh

    for(int j = 0; j < n; j++){

        if(g[minVertex][j] != 0 && visited[j] != 1){

            if(g[minVertex][j] < weight[j])
                {
                    weight[j] = g[minVertex][j];
                    parent[j] = minVertex;
                }

        }
    }


    }



   int sum = 0;
        System.out.println("Edge \tWeight"); 
        for (int i = 1; i < n; i++) {
            System.out.println(parent[i] + " - " + i + "\t" + g[i][parent[i]]); 
            sum = sum  + g[i][parent[i]];
        }
System.out.println(sum);
}

//main

public static void main(String[] args) {
  
    int g[][] = { { 0, 3, 4, 0, 0 }, 
    { 0, 0, 5, 0, 0 }, 
    { 0, 6, 0, 0, 0 }, 
    { 0, 0, 0, 7, 0 }, 
    { 0, 0, 2, 0, 0 } }; 

// int g[][] = new int[5][5];

// g[1-1][2-1] = 3;
// g[1-1][3-1] = 4;
// g[4-1][2-1] = 6;
// g[5-1][2-1] = 2;
// g[2-1][3-1] = 5;
// g[3-1][5-1] = 7;



int n = 5;

prim(n,g);

}

}