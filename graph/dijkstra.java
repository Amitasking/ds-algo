/**
 * @author: Amit Thakur
 * DIJKSTRA'S ALGORITHM
 */

import java.util.Collection;
import java.util.Arrays;
import java.util.TreeSet;
import java.util.SortedSet;

class hh {

   static void dikstra(int[][] graph, int v, int source) {
    
    SortedSet<Integer> s = new TreeSet<>();
    int[] distance =new int[v];
    Arrays.fill(distance, Integer.MAX_VALUE);

    distance[source] = 0;
    s.add(source);
    boolean[] visited = new boolean[v];
    visited[source]  =true; 

   
    while(!s.isEmpty()) {
        ///remove min 
        int node = s.first();
        s.remove(node);
        visited[node] = true;
        //explore neighbours
        for(int i = 0; i< v; i++) {
            if(graph[node][i] != 0 && visited[i] != true){
                s.add(i);
                int newDistance = distance[node] + graph[node][i];
                if(newDistance < distance[i]){
                    distance[i] = newDistance;
                    
                }
            }

        }


    }
    
        System.err.println(Arrays.toString(distance));

   }
   
    public static void main(String[] args) {
        int v = 5;
        // int e = 4;
        int[][] graph = new int[v][v];
        // INIT GRAPH

        for(int i = 0; i < v; i++) {
            for(int j = 0; j < v; j++) {
                graph[i][j] = 0; 
        }}

         graph[1][2] =  24;
         graph[1][4] = 20;
         graph[3][1] =  3;
         graph[4][3] =  12;
         
         graph[3][4] =  12;
         graph[1][3] =  3;
         graph[2][1] =  24;
         graph[4][1] = 20;
         
         
        
       
         dikstra(graph,v,1);
///////to re-move





}}
