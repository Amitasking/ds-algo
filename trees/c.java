import java.util.LinkedList;
import java.util.Queue;

class c {


    static class cell {
        int x;
        int y;
        int time;
        boolean visited;
        cell(){}

        cell(int x, int y ){
            this.x = x;
            this.y = y;
        }

        cell(int x, int y , int time, boolean visited){
            this.x = x;
            this.y = y;
            this.time = time;
            this.visited = visited;
        }
    }

    public static boolean  isValid(int x,int y) {
        int R = 3;
        int C = 5;
        return ( (x >= 0 &&  y >= 0) && (x < R && y < C) );
    }

    public static void bfs(int[][] matrix, int R, int C){
        // boolean[] visited = new boolean[n];
        Queue<cell> q = new LinkedList<>();

        //store all sources

        for(int i = 0;  i< matrix.length; i++ ){
            for(int j =0; j< matrix[0].length; j++){
                if(matrix[i][j] == 2)
                    q.add(new cell(i,j));
            }

        }
        cell temp = new cell();

        while(!q.isEmpty()){
          temp = q.poll();

            //addd left neighbours

            if(isValid(temp.x-1, temp.y)  && matrix[temp.x-1][temp.y] == 1){
                q.add(new cell(temp.x, temp.y, temp.time+1, true));
                matrix[temp.x-1][temp.y] = 2;
            
            }

            //right

            if(isValid(temp.x+1, temp.y)  &&  matrix[temp.x+1][temp.y] == 1){
                q.add(new cell(temp.x+1, temp.y, temp.time+1, true));
                 matrix[temp.x+1][temp.y] = 2;
            
            }

            //top
            if(isValid(temp.x, temp.y+1) && matrix[temp.x][temp.y+1] == 1){
                q.add(new cell(temp.x, temp.y, temp.time+1, true));
                matrix[temp.x][temp.y+1] = 2;
            
            }


            //down

            //top
            if(isValid(temp.x, temp.y-1) &&  matrix[temp.x][temp.y-1] == 1){
                q.add(new cell(temp.x, temp.y, temp.time+1, true));
                matrix[temp.x][temp.y-1] = 2;
            }
            
        }
        
        for(int i = 0; i< R; i++){
            for(int j = 0 ; j< C; j++){
                if(matrix[i][j] == 1)
                    System.out.println("no way");
            }
        }
        System.out.println(temp.x +" " + temp.y + ":"+ temp.time);
       
    }

    public static void main(String[] args) {
        int[][] matrix = { {2, 1, 0, 2, 1}, 
        {1, 0, 1, 2, 1}, 
        {1, 0, 0, 2, 1}};         
      

        // { { 2, 1, 0, 2, 1 }, 
        // { 0, 0, 1, 2, 1 }, 
        // { 1, 0, 0, 2, 1 } }; 


      
        int R = 3;
        int C = 5;
        bfs(matrix,R,C); 

    }


   
}