import java.util.Arrays;

class primss {
//    static void printMST(int parent[], int graph[][], int n) 
//     { 
//         System.out.println("Edge \tWeight"); 
//         for (int i = 1; i < n; i++) 
//             System.out.println(parent[i] + " - " + i + "\t" + graph[i][parent[i]]); 
//     } 
    //return min

    public static int minVertex(int[] visited, int[] weights){
        int minIndex = -1;
        int minValue = Integer.MAX_VALUE;

        for(int i = 0; i < weights.length; i++){
            if(visited[i] != 1 && (minIndex == -1 || weights[i] < weights[minIndex] ))
                minIndex = i;
            }
        return minIndex;

    }


    public static void prims(int[][] g, int n){
        int[] visited = new int[n];
    int[] weights = new int[n];
    int[] parent = new int[n];
    Arrays.fill(weights, Integer.MAX_VALUE);
    Arrays.fill(parent,-1);


    //source

    parent[0] = -1;
    weights[0] = 0;

    
    //loop


    for(int i = 0; i < n; i++){
        //find min vertex

        int min = minVertex(visited, weights);
        visited[min] = 1;

        //explore

        for(int j = 0; j < n; j++){
            if(g[min][j] != 0 && visited[j] != 1){
                if(g[min][j] < weights[j]){
                    weights[j] = g[min][j];
                    parent[j] = min; 
                    visited[j]  = 1;
                }
            }
        }



    }

System.out.println(Arrays.toString(parent));
//printMST(parent,g,n);


    }

    

    public static void main(String[] args) {
    int g[][] = { { 0, 2, 0, 6, 0 }, 
    { 2, 0, 3, 8, 5 }, 
    { 0, 3, 0, 0, 7 }, 
    { 6, 8, 0, 0, 9 }, 
    { 0, 5, 7, 9, 0 } }; 
    int n = 5;

    prims(g,n);
    }
}