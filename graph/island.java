import java.util.Queue;
import java.util.LinkedList;

class Island {
    
    public static boolean valid(int i, int j ,int R , int C){
        return( i < R && j < C ) && ( i>=0 && j >= 0 );
    }



    static class cell{
        int x;
        int y;
        
        cell(int x, int y){
            this.x = x;
            this.y = y;
        }
        cell(){}
    }

    public static void bfs(int[][] g, int si, int sj, int[][] visited, int R, int C) {
         

            Queue<cell> q = new LinkedList<>();
            q.add(new cell(si,sj));
            visited[si][sj] = 1;
            cell temp = new cell();
            int count = 1;
            while (!q.isEmpty()) {
                temp = q.poll();
               System.out.println(temp.x + " "+ temp.y);

                //top
                if(valid(temp.x-1,temp.y,R,C) && (  g[temp.x-1][temp.y] == 1 && visited[temp.x-1][temp.y] != 1) ){
                    q.add(new cell(temp.x-1, temp.y));
                    visited[temp.x-1][temp.y] = 1; 
                    count++;
                
                }
                //down 
                if(valid(temp.x+1,temp.y,R,C) && (  g[temp.x+1][temp.y] == 1 && visited[temp.x+1][temp.y] != 1) ){
                    q.add(new cell(temp.x+1, temp.y));
                    visited[temp.x+1][temp.y] = 1; 
                    count++;
                }

                //right

                if(valid(temp.x,temp.y+1,R,C) && (  g[temp.x][temp.y+1] == 1 && visited[temp.x][temp.y+1] != 1) ){
                    q.add(new cell(temp.x, temp.y+1));
                    visited[temp.x][temp.y+1] = 1; 
                    count++;
                
                }


                //left

                if(valid(temp.x,temp.y-1,R,C) && (  g[temp.x][temp.y-1] == 1 && visited[temp.x][temp.y-1] != 1) ){
                    q.add(new cell(temp.x, temp.y-1));
                    visited[temp.x][temp.y-1] = 1; 
                    count++;
                
                }

                //left top


                if(valid(temp.x+1,temp.y-1,R,C) && (  g[temp.x+1][temp.y-1] == 1 && visited[temp.x+1][temp.y-1] != 1) ){
                    q.add(new cell(temp.x+1, temp.y-1));
                    visited[temp.x+1][temp.y-1] = 1; 
                    count++;
                
                }

                //right top
                if(valid(temp.x+1,temp.y+1,R,C) && (  g[temp.x+1][temp.y+1] == 1 && visited[temp.x+1][temp.y+1] != 1) ){
                    q.add(new cell(temp.x+1, temp.y+1));
                    visited[temp.x+1][temp.y+1] = 1; 
                    count++;
                
                }

                //right bottom

                if(valid(temp.x-1,temp.y+1,R,C) && (  g[temp.x-1][temp.y+1] == 1 && visited[temp.x-1][temp.y+1] != 1) ){
                    q.add(new cell(temp.x-1, temp.y+1));
                    visited[temp.x-1][temp.y+1] = 1; 
                    count++;
                }
                

                //left bott

                if(valid(temp.x-1,temp.y-1,R,C) && (  g[temp.x-1][temp.y-1] == 1 && visited[temp.x-1][temp.y-1] != 1) ){
                    q.add(new cell(temp.x-1, temp.y-1));
                    visited[temp.x-1][temp.y-1] = 1; 
                    count++;
                }
                







            }

    

            System.out.println("count = " + count);
        

    }


    public static void main(String[] args) {
        // int mat[][] = { { 1, 1, 0, 0, 0 }, 
        //                 { 0, 1, 0, 0, 1 }, 
        //                 { 1, 0, 0, 1, 1 }, 
        //                 { 0, 0, 0, 0, 0 }, 
        //                 { 1, 0, 1, 0, 1 } }; 
int R = 5;
int C = 5;

                        int mat[][] = { { 1, 1, 0, 0,   0  }, 
                                        { 0, 1, 1, 0,0 }, 
                                        { 0, 0, 1, 0, 1 }, 
                                        { 1, 0, 0, 0,1 }, 
                                        { 0, 1, 0, 1 ,1 } }; 



        // int mat[][] = { { 1, 1, 0, 0}, 
        // { 1, 1, 0, 0}, 
        // { 0, 1, 1, 0}, 
        // { 0, 0, 1, 0}, 
        // { 1, 0, 0, 0} }; 



                        int visited[][] = new int[R][C];
                        int res = 0;
                        for(int i = 0;i < R; i++){
                            for(int j = 0;j < C; j++){
                                if(visited[i][j] != 1 && mat[i][j] == 1 ){
                                    bfs(mat,i,j,visited,R,C);
                                    res++;
                                }
                            }
                        }
            System.out.println("islands = " + res);
                          
    }


}