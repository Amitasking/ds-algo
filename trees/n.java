import java.util.LinkedList;
import java.util.Queue;
import java.util.Arrays;


class n {


    static class cell {
        int x;
        int y;
        int d;
        cell(int x, int y){
            this.x = x;
            this.y = y;
        }
        cell(int x, int y, int d){
            this.x = x;
            this.y = y;
            this.d = d;
        }
        
    }

    public static boolean isValid(int x, int y){
        int R = 3;
        int C = 3;
        return( ( x >= 0 &&  y >=0) && ( x < R &&  y < C) );
    }



    public static void bfs(char[][] matrix) {
        Queue<cell> q = new LinkedList<>();
        int[][] distance = new int[3][3];


        q.add(new cell(0,0,0));
        matrix[0][0] = 'X';

        cell temp = new cell(0,0);
        while (!q.isEmpty()) {
            System.out.print("("+q.peek().x+ ","+q.peek().y +")" + "--> "  );
            temp = q.poll();

            ///top
            if(isValid(temp.x, temp.y+1) && matrix[temp.x][temp.y+1] == '.'){
                matrix[temp.x][temp.y+1] = 'X';
                q.add(new cell(temp.x, temp.y+1, temp.d+1));
                distance[temp.x][temp.y+1] = temp.d+1;

            }


            //down

            if(isValid(temp.x, temp.y-1) && matrix[temp.x][temp.y-1] == '.'){
                matrix[temp.x][temp.y-1] = 'X';
                q.add(new cell(temp.x, temp.y-1, temp.d+1));
                distance[temp.x][temp.y-1] = temp.d+1;

            }


            //left


            if(isValid(temp.x-1, temp.y) && matrix[temp.x-1][temp.y] == '.'){
                matrix[temp.x-1][temp.y] = 'X';
                q.add(new cell(temp.x-1, temp.y, temp.d+1));
                distance[temp.x-1][temp.y] = temp.d+1;

            }

            //right
            if(isValid(temp.x+1, temp.y) && matrix[temp.x+1][temp.y] == '.'){
                matrix[temp.x+1][temp.y] = 'X';
                q.add(new cell(temp.x+1, temp.y, temp.d+1));
                distance[temp.x+1][temp.y] = temp.d+1;

            }

            //


        }
        
//System.out.println(temp.x + ""+ temp.y+  ":" + temp.d );
System.out.println(Arrays.deepToString(distance));
    }
    public static void main(String[] args) {
        char[][] a = {{'.', 'X',  '.'},
                    {'.', 'X',  '.'},
                    {'.', '.',  '.'}};
        
        bfs(a);
    
    }


}   